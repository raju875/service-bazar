<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//administration
Route::get('/front/fetch-publish-services','ApiController@apiPublishServicesFetch');

Route::get('/admin-panel/login-form', 'AdministrationController@administrationLoginForm');
Route::post('/admin-panel/administration-login', 'AdministrationController@administrationLogin');
Route::post('/admin-panel/logout', 'AdministrationController@administrationLogout')->middleware('AuthenticateMiddleware');

Route::get('/admin-panel/administration/administration-registration-form', 'AdministrationController@registrationForAdministration')->middleware('SuperAdminMiddleware');
Route::post('/admin-panel/administration/administration-sign-up', 'AdministrationController@signUpAdministration')->middleware('SuperAdminMiddleware');
Route::get('/admin-panel/administration/list-of-administration', 'AdministrationController@listOfAdministration')->middleware('SuperAdminMiddleware');
Route::get('/admin-panel/administration/unblock-admin/{id}', 'AdministrationController@unblockAdmin')->middleware('SuperAdminMiddleware');
Route::get('/admin-panel/administration/block-admin/{id}', 'AdministrationController@blockAdmin')->middleware('SuperAdminMiddleware');
Route::get('/admin-panel/administration/delete-admin-info/{id}', 'AdministrationController@deleteAdminInfo')->middleware('SuperAdminMiddleware');


Route::view('/admin-panel/dashboard', 'admin.dashboard.dashboard')->middleware('AuthenticateMiddleware');
Route::view('/admin-panel/home', 'admin.dashboard.dashboard')->middleware('AuthenticateMiddleware');
//Route::get('/admin-panel/dashboard', 'DashboardController@dashboard')->middleware('AuthenticateMiddleware');
//Route::get('/admin-panel/home', 'DashboardController@dashboard')->middleware('AuthenticateMiddleware');

//profile
Route::get('/admin-panel/profile/show-profile', 'AdminProfileController@showProfile')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/profile/update-profile', 'AdminProfileController@updateProfile')->middleware('AuthenticateMiddleware');


//activity logs
Route::get('/admin-panel/list/activity-log', 'ActivityController@listOfActivities')->middleware('AuthenticateMiddleware');


//division info
Route::get('/admin-panel/area/division-form', 'AreaController@divisionForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/area/division-create', 'AreaController@createDivision')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/area/division-table', 'AreaController@manageDivision')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/area/unpublished-division/{id}', 'AreaController@unpublishedDivision')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/area/published-division/{id}', 'AreaController@publishedDivision')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/area/editable-division-form/{id}', 'AreaController@divisionEditableForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/area/update-division-info', 'AreaController@divisionInfoUpdate')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/area/delete-division-info/{id}', 'AreaController@divisionInfoDelete')->middleware('AuthenticateMiddleware');


//thana info
Route::get('/admin-panel/area/thana-form', 'AreaController@thanaForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/area/thana-create', 'AreaController@createThana')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/area/thana-table', 'AreaController@manageThana')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/area/unpublished-thana/{id}', 'AreaController@unpublishedThana')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/area/published-thana/{id}', 'AreaController@publishedThana')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/area/editable-thana-form/{id}', 'AreaController@thanaEditableForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/area/update-thana-info', 'AreaController@thanaInfoUpdate')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/area/delete-thana-info/{id}', 'AreaController@thanaInfoDelete')->middleware('AuthenticateMiddleware');


//category info
Route::get('/admin-panel/category/category-form', 'CategoryController@categoryForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/category/category-create', 'CategoryController@createCategory')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/category/category-table', 'CategoryController@manageCategory')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/category/unpublished-category/{id}', 'CategoryController@unpublishedCategory')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/category/published-category/{id}', 'CategoryController@publishedCategory')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/category/editable-category-form/{id}', 'CategoryController@categoryEditableForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/category/update-category-info', 'CategoryController@categoryInfoUpdate')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/category/delete-category-info/{id}', 'CategoryController@categoryInfoDelete')->middleware('AuthenticateMiddleware');


//service point info
Route::get('/admin-panel/service-point/service-point-form', 'ServicePointController@servicePointForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/service-point/service-point-create', 'ServicePointController@createServicePoint')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service-point/service-point-table', 'ServicePointController@manageServicePoint')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service-point/unpublished-service-point/{id}', 'ServicePointController@unpublishedServicePoint')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service-point/published-service-point/{id}', 'ServicePointController@publishedServicePoint')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service-point/editable-service-point-form/{id}', 'ServicePointController@servicePointEditableForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/service-point/update-service-point-info', 'ServicePointController@servicePointInfoUpdate')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service-point/delete-service-point-info/{id}', 'ServicePointController@servicePointInfoDelete')->middleware('AuthenticateMiddleware');


//service info
Route::get('/admin-panel/service/service-form', 'ServiceController@serviceForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/service/service-create', 'ServiceController@createService')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service/service-table', 'ServiceController@manageService')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service/unpublished-service/{id}', 'ServiceController@unpublishedService')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service/published-service/{id}', 'ServiceController@publishedService')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service/editable-service-form/{id}', 'ServiceController@serviceEditableForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/service/update-service-info', 'ServiceController@serviceInfoUpdate')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service/delete-service-info/{id}', 'ServiceController@serviceInfoDelete')->middleware('AuthenticateMiddleware');


//service feature info
Route::get('/admin-panel/service-feature/service-feature-form', 'ServiceFeatureController@serviceFeatureForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/service-feature/service-feature-create', 'ServiceFeatureController@createServiceFeature')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service-feature/service-feature-table', 'ServiceFeatureController@manageServiceFeature')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service-feature/unpublished-service-feature/{id}', 'ServiceFeatureController@unpublishedServiceFeature')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service-feature/published-service-feature/{id}', 'ServiceFeatureController@publishedServiceFeature')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service-feature/editable-service-feature-form/{id}', 'ServiceFeatureController@serviceFeatureEditableForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/service-feature/update-service-feature-info', 'ServiceFeatureController@serviceFeatureInfoUpdate')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/service-feature/delete-service-feature-info/{id}', 'ServiceFeatureController@serviceFeatureInfoDelete')->middleware('AuthenticateMiddleware');

//banner info
Route::get('/admin-panel/banner/banner-form', 'BannerController@bannerForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/banner/banner-create', 'BannerController@createBanner')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/banner/banner-table', 'BannerController@manageBanner')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/banner/unpublished-banner/{id}', 'BannerController@unpublishedBanner')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/banner/published-banner/{id}', 'BannerController@publishedBanner')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/banner/editable-banner-form/{id}', 'BannerController@bannerEditableForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/banner/update-banner-info', 'BannerController@bannerInfoUpdate')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/banner/delete-banner-info/{id}', 'BannerController@bannerInfoDelete')->middleware('AuthenticateMiddleware');


//worker info
Route::get('/admin-panel/worker/worker-list', 'WorkerController@listOfWorker')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/worker/unpublished-worker/{id}', 'WorkerController@unpublishedWorker')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/worker/published-worker/{id}', 'WorkerController@publishedWorker')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/worker/delete-worker-info/{id}', 'WorkerController@workerInfoDelete')->middleware('AuthenticateMiddleware');

Route::get('/admin-panel/worker/worker-details/{id}', 'WorkerController@workerDetails')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/worker/worker-table', 'WorkerController@manageWorker')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/worker/editable-worker-form/{id}', 'WorkerController@workerEditableForm')->middleware('AuthenticateMiddleware');
Route::post('/admin-panel/worker/update-worker-info', 'WorkerController@bannerInfoUpdate')->middleware('AuthenticateMiddleware');


// user info
Route::get('/admin-panel/user/user-list', 'UserController@userList')->middleware('AuthenticateMiddleware');
Route::get('/admin-panel/user/delete-user-info/{id}', 'UserController@deleteUserInfoById')->middleware('AuthenticateMiddleware');

// order details
Route::get('/admin-panel/order/or/admin-panel/service-feature/service-feature-tableder-list', 'OrderController@orderList')->middleware('AuthenticateMiddleware');


//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');


// ((((((((((((((((((((((((((( __ERROR__ ))))))))))))))))))))))))))) //
Route::get('/404', 'ErrorController@error404');
Route::get('/405', 'ErrorController@error405');
// ((((((((((((((((((((((((((( __ERROR__ ))))))))))))))))))))))))))) //




// ((((((((((((((((((((((((((( __FRONT__ ))))))))))))))))))))))))))) //

//registration worker
Route::get('/signup', 'FrontRegistrationLoginController@signupPage');
Route::get('/worker-register1', 'FrontRegistrationLoginController@workerRegisterFirstPage');
Route::post('/worker-register2', 'FrontRegistrationLoginController@workerRegisterSecondPage');
Route::get('/ajax-thana-list/{id}', 'FrontRegistrationLoginController@ajaxThanaList');
Route::post('/personal-info', 'FrontRegistrationLoginController@workerRegisterThirdPage');
Route::post('/save-info', 'FrontRegistrationLoginController@saveInfo');
Route::get('/my-profile', 'FrontRegistrationLoginController@showMyProfile');

//registration for hire
Route::get('/user-register', 'FrontRegistrationLoginController@userRegisterPage');
Route::post('/save-user-info', 'FrontRegistrationLoginController@userInfoSave');
Route::get('/user-signout', 'FrontRegistrationLoginController@userSignout');
Route::get('/user-signout', 'FrontRegistrationLoginController@userSignout');
Route::get('/login', 'FrontRegistrationLoginController@login');
Route::post('/login-submit', 'FrontRegistrationLoginController@loginSubmit');


Route::get('/', 'FrontController@homePage');
Route::get('/home-service', 'FrontController@homePage');
Route::get('/services', 'FrontController@servicePage');
