<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// user registration
Route::post('/front/save-user-info','ApiUserController@apiSaveUserInfo');
Route::post('/front/login-user','ApiUserController@apiLoginUser');
//home page
Route::get('/front/fetch-publish-categories-for-sidebar','ApiController@apiPublishCategoriesFetchForSidebar');
Route::get('/front/fetch-publish-popular-services','ApiController@apiPublishForPopularServices');
Route::get('/front/fetch-publish-banners-for-carousel','ApiController@apiPublishBannersFetchForCarousel');
Route::get('/front/fetch-publish-services-for-home','ApiController@apiPublishServiceForHome');
Route::get('/front/fetch-publish-services-for-office','ApiController@apiPublishServiceForOffice');

//service page
Route::get('/front/fetch-all-publish-services','ApiServiceController@apiFetchALlServices');
Route::get('/front/fetch-publish-category-by-id/{categoryId}','ApiServiceController@apiFetchServiceCategoryById');


//search here
Route::get('/front/query-for-global-searching','ApiSearchController@apiQueryForGlobalSearching');
Route::get('/front/select-location-by-division/{divisionName}','ApiSearchController@apiLocationSelectByDivision');
Route::get('/front/select-location-by-thana/{divisionName}/{thanaName}','ApiSearchController@apiLocationSelectByThana');


//service feature here
Route::get('/front/fetch-publish-service-feature/{id}','ApiServiceFeatureController@apiFetchServiceFeature');


// add to cart
Route::get('/front/fetch-publish-service-feature-by-id/{id}','ApiAddToCartController@apiFetchServiceFeatureById');
Route::get('/front/fetch-publish-location','ApiAddToCartController@apiFetchServiceLocation');
Route::get('/front/fetch-freelancer-by-select-location/{location}/{serviceId}','ApiAddToCartController@apiFetchFreelancerBySelectLocation');


// footer content
Route::get('/front/fetch-all-publish-categories','ApiFooterController@apiFooterAllPublishCategory');

//=============================    ==============================    ================================

//freelancer login/registration/logout
Route::post('/front/freelancer-register-mobile-input-validation/{mobile}','ApiController@apiMobileValidationRegistrationFreelancer');
Route::get('/front/freelancer-login-mobile-id-input-validation/{idMobile}','ApiController@apiMobileIdValidationLoginFreelancer');
Route::get('/front/freelancer-login-input-password-validation/{mobile}/{password}','ApiController@apiPasswordValidationLoginFreelancer');
Route::post('/registration/freelancer','ApiController@apiRegistrationFreelancer');
Route::post('/login/freelancer','ApiController@apiLoginFreelancer');
Route::post('front/worker-logout','ApiController@apiLogoutFreelancer');


//freelancer profile add/edit/delete job field
Route::get('/front/fetch-publish-divisions','ApiController@apiPublishDivisions');
Route::get('/front/fetch-publish-thanas','ApiController@apiPublishThanas');
Route::get('/front/fetch-select-service-for-price/{id}','ApiController@apiServiceFeaturePriceById');
Route::post('/front/save-freelancer-job-field','ApiController@apiSaveFreelancerJobField');
Route::post('/front/delete-freelancer-job-field','ApiController@apiDeleteFreelancerJobField');
Route::get('/front/show-freelancer-job-field/{workerId}','ApiController@apiShowFreelancerJobField');
Route::post('/front/update-freelancer-profile-image','ApiController@apiFreelancerImageUpdate');
Route::get('/front/freelancer-edit-job-field/{categoryId}/{serviceId}/{serviceFeatureId}/{price}/{workerId}','ApiController@apiFreelancerEditJobField');
Route::post('/front/update-freelancer-job-field','ApiController@apiFreelancerUpdateJobField');
Route::post('/front/delete-freelancer-job-field-by-id','ApiController@apiFreelancerDeleteJobField');
Route::get('/front/fetch-publish-skills-for-search','ApiController@apiPublishSkillsFetch');



//Route::get('/front/fetch-publish-categories-for-popular-services','ApiController@apiPublishCategoriesFetchForPopularServices');
Route::get('/front/fetch-publish-division-for-search','ApiController@apiPublishDivisionFetchForSearch');
Route::get('/front/fetch-publish-thanas-for-search/{id}','ApiController@apiPublishThanaFetchForSearch');
Route::get('/front/fetch-publish-services/{id}','ApiController@apiPublishServicesFetch');
Route::get('/front/fetch-publish-service-features','ApiController@apiPublishServiceFeaturesFetch');



Route::get('/front/fetch-publish-freelancer-info-by-id/{id}','ApiController@apiFreelancerInfoById');
Route::get('/front/fetch-publish-skill','ApiController@apiFreelancerSkillFetch');
Route::get('/front/fetch-publish-service-by-id/{id}','ApiController@apiFreelancerServiceFetchById');
Route::get('/front/fetch-select-service-for-service-feature/{id}','ApiController@apiFreelancerServiceFeatureFetchByServiceId');




