<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkerInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worker_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('worker_id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('password');
            $table->string('mobile',100)->unique();
            $table->string('image')->default('front/profile-images/default.jpg');
            $table->string('address');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worker_infos');
    }
}
