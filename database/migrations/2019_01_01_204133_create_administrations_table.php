<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email',190)->unique();
            $table->string('password');
            $table->string('mobile');
            $table->tinyInteger('status');
            $table->string('avatar')->default('default.jpg');
            $table->string('role');
            $table->timestamps();
        });
        DB::table('administrations')->insert(
            array(
                'name' => 'Al Amin Raju',
                'email' => 'raju@gmail.com',
                'password' => Hash::make('123456'),
                'mobile' => '01827347779',
                'status' => '1',
                'avatar' => 'default.jpg',
                'role' => 'super_admin',
                'created_at' => now(),
                'updated_at' => now(),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrations');
    }
}
