<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreelancerWorkingAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freelancer_working_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('worker_id');
            $table->integer('service_id');
            $table->integer('service_feature_id');
            $table->integer('division_id');
            $table->integer('thana_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freelancer_working_areas');
    }
}
