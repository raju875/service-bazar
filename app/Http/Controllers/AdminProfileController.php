<?php

namespace App\Http\Controllers;

use App\administration;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Session;
class AdminProfileController extends Controller
{
    public function showProfile() {
        return view('admin.profile.show-profile');
    }

    public function updateProfile(Request $request){
        //Handle and update admin profile image

        //return $request->all();
        if ($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $fileName = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 200)->save(public_path('admin/profile-pictures/'.$fileName ));

            $admin = administration::find(Session::get('adminId'));
            $admin->avatar = $fileName;
            $admin->save();

            return view('admin.profile.show-profile' );

        }

    }
}
