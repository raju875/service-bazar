<?php

namespace App\Http\Controllers;

use App\Administration;
use Illuminate\Http\Request;
use App\ActivityLog;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class AdministrationController extends Controller
{

    public function administrationLoginForm() {
        return view('admin.login.login');
    }

    public function administrationLogin(Request $request) {
        //return $request->all();
        $res1 = $request->res1;
        $res2 = $request->res2;
        if ($res1!=$res2 ) {
            return redirect('/admin-panel/login-form')->with('alert','wrong answer!!!try again');
        } else {

            $countLogin = Administration::where('email',$request->email)->count();
            if ($countLogin == 0 ) {
                return redirect('/admin-panel/login-form')->with('alert','Invalid email!!!try again');
            } else {
                $admin =  Administration::where('email',$request->email)->first();
                if ($admin->status == 1 ) {
                    if ($admin ) {
                        if (password_verify($request->password,$admin->password)) {
                            Session::put('adminEmail',$admin->email);
                            Session::put('adminId',$admin->id);
                            Session::put('adminName',$admin->name);
                            Session::put('adminRole',$admin->role);
                            return redirect('/admin-panel/dashboard');
                        } else {
                            return redirect('/admin-panel/login-form')->with('alert','Wrong password!!!try again');
                        }
                    }
                } else {
                    return redirect('/admin-panel/login-form')->with('alert','Invalid email!!!try again');
                }
            }
        }
    }

    public function registrationForAdministration()
    {
        return view('admin.administration.registration-for-administration');
    }

    public function signUpAdministration(Request $request)
    {
        $res1 = $request->res1;
        $res2 = $request->res2;
        $password = $request->password;
        $rePassword = $request->repassword;
        if ($password != $rePassword ) {
            return redirect('/admin-panel/administration/administration-registration-form')->with('message','confirmation password is wrong!!!try again');
        }
        if ($res1!=$res2 ) {
            return redirect('/admin-panel/administration/administration-registration-form')->with('alert','wrong answer!!!try again');
        } else {
            $countRegister = Administration::where('email',$request->email)->count();
            if($countRegister == 0 ) {
                $login = new Administration();
                $login->name = $request->name;
                $login->email = $request->email;
                $login->password = bcrypt($request->password);
                $login->mobile = $request->mobile;
                $login->role = $request->role;
                $login->save();

                $activity = new ActivityLog();
                $activity->description = Session::get('adminName')." have created new entry successfully";
                $activity->table = "administration";
                $activity->id_no =$login->id ;
                $activity->user = Session::get('adminEmail');
                $activity->save();
                return redirect('/admin-panel/administration/list-of-administration')->with('message','Add new administration successfully');
            } else {
                return redirect('/admin-panel/administration/administration-registration-form')->with('alert','Email has already used !!! try another email');
            }
        }
    }


    public function listOfAdministration()
    {
        return view('admin.administration.list-of-administration');
    }


    public function unblockAdmin($id)
    {
        DB::table('administrations')->where('id', $id)->update(['status' => 1]);

        $activity = new ActivityLog();
        $activity->description = "You have unblocked entry successfully";
        $activity->table = "administrations";
        $activity->id_no = $id;
        $activity->user = Session::get('adminEmail');
        $activity->save();
        return redirect('/admin-panel/administration/list-of-administration')->with('message', "Unblock admin successfully");
    }

    public function blockAdmin($id)
    {
        DB::table('administrations')->where('id', $id)->update(['status' => 0]);

        $activity = new ActivityLog();
        $activity->description = "You have blocked entry successfully";
        $activity->table = "administrations";
        $activity->id_no = $id;
        $activity->user = Session::get('adminEmail');
        $activity->save();
        return redirect('/admin-panel/administration/list-of-administration')->with('message', "Block admin successfully");

    }

    public function deleteAdminInfo($id)
    {
        Admnistration::find($id)->delete();
        $activity = new ActivityLog();
        $activity->description = "You have deleted entry successfully";
        $activity->table = "administrations";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();
        return redirect('/admin-panel/administration/list-of-administration')->with('message', 'Admin info delete successfully');
    }

    public function administrationLogout() {
        Session::forget('adminEmail');
        Session::forget('adminName');
        Session::forget('adminId');
        Session::forget('adminRole');
        return redirect('/admin-panel/login-form')->with('message','Logout successfully');
    }
}
