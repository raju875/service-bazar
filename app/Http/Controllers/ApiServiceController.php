<?php

namespace App\Http\Controllers;

use App\Category;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiServiceController extends Controller
{

    public function apiFetchALlServices () {
       return Service::where('status',1)->orderBy('id','desc')->get();
    }

    public function apiFetchServiceCategoryById ($categoryId) {
       return DB::table('services')
           ->join('categories','services.category_id','=','categories.id')
           ->select('categories.category_name','services.*')
           ->where('services.status',1)
           ->where('categories.id',$categoryId)
           ->get();
    }
}
