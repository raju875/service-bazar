<?php

namespace App\Http\Controllers;

use App\ActivityLog;
use App\User;
use App\WorkerInfo;
use Illuminate\Http\Request;

class ApiUserController extends Controller
{
    public function apiSaveUserInfo(Request $request) {
       $newUser = new User();
       $newUser->name = $request->name;
       $newUser->mobile = $request->mobile;
       $newUser->email = $request->email;
       $newUser->password = bcrypt($request->password);
       $newUser->save();

       $activity = new ActivityLog();
       $activity->description = '(User) registration successfully';
       $activity->table = "users";
       $activity->id_no =$newUser->id ;
       $activity->user = $request->name;
       $activity->save();

       $token = $newUser->createToken('Se4So')->accessToken;
       return response()->json(['token' => $token, 'newUser' => $newUser ], 200);
    }


    public function apiLoginUser(Request $request) {
        // query and check by input mobile number
        $userByEmail = WorkerInfo::where('email',$request->email)->first();
        if ($userByEmail ) {
            if (password_verify($request->password,$userByEmail->password)) {
                $activity = new ActivityLog();
                $activity->description = '(User) login successfully';
                $activity->table = "users";
                $activity->id_no =$userByEmail->id ;
                $activity->user = $userByEmail->name;
                $activity->save();
                $token = $userByEmail->createToken('Se4So')->accessToken;
                return response()->json(['token' => $token , 'worker' => $userByEmail ], 200);
            }
            $token = $userByEmail->createToken('Se4So')->accessToken;
            return response()->json(['token' => $token, 'newUser' => $userByEmail ], 200);
        }
    }
}
