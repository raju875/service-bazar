<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Category;
use App\Division;
use App\FreelancerJobField;
use App\FreelancerWorkingArea;
use App\Service;
use App\ServiceFeature;
use App\ServicePointForService;
use App\Thana;
use App\WorkerInfo;
use Illuminate\Http\Request;
use Illuminate\Contracts\Session;
use App\ActivityLog;
use Illuminate\Auth;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function apiPublishCategoriesFetchForSidebar() {
       return Category::where('status',1)->get();
    }

    public function apiPublishBannersFetchForCarousel() {
       return Banner::where('status',1)->get();
    }

    public function apiPublishForPopularServices() {
       return Category::where('status',1)->get();
    }

    public function apiPublishDivisionFetchForSearch() {
       return Division::where('status',1)->orderBy('id','desc')->get();
    }

    public function apiPublishThanaFetchForSearch($id) {
       return Thana::where('division',$id)->where('status',1)->get();
    }

    public function apiPublishSkillsFetch() {
       return Category::where('status',1)->get();
    }

    public function apiPublishServicesFetch($id) {
       return Service::where('category_id',$id)->where('status',1)->get();
    }

    public function apiPublishServiceFeaturesFetch() {
       return ServiceFeature::where('status',1)->get();
    }

   public function apiMobileValidationRegistrationFreelancer($mobile) {
        $count = WorkerInfo::where('mobile',$mobile)->count();
        if ($count == 0 ) {
            return 'ok';
        }else {
            return 'duplicateMobileInput';
        }
    }

    public function apiMobileIdValidationLoginFreelancer($mobileId) {
        $freelancerByMobile = WorkerInfo::where('mobile',$mobileId)->where('status',1)->first();
        if ($freelancerByMobile) {
            return 'mobileFound';
        }
        $freelancerById = WorkerInfo::where('worker_id',$mobileId)->where('status',1)->count();
        if ($freelancerById) {
            return 'idFound';
        }
        return 'notFound';
    }

     public function apiPasswordValidationLoginFreelancer($data,$password) {
         $freelancerByMobile =  WorkerInfo::where('mobile',$data)->where('status',1)->first();
         if ($freelancerByMobile) {
            if (password_verify($password,$freelancerByMobile->password)) {
                return 'mobilePasswordFound';
            }
        }
         $freelancerById = WorkerInfo::where('worker_id',$data)->where('status',1)->first();
         if ($freelancerById) {
             if (password_verify($password,$freelancerById->password)) {
                 return 'idPasswordFound';
             }
         }
         return 'notFound';
    }

    public function apiRegistrationFreelancer(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|min:6|max:20',
            'retype_password' => 'required',
            'mobile' => 'required',
            'address' => 'required|max:200',
        ]);
        if ($request->password != $request->retype_password ) {
            return 'ConfirmPasswordError';
        }
        $count = WorkerInfo::where('mobile',$request->mobile)->count();
        if (!$count == 0) {
            return 'duplicateMobileInput';
        }
            $count = WorkerInfo::count();
            $id = 'se4so-'.($count+1);
            $worker = new WorkerInfo();
            $worker->worker_id = $id;
            $worker->name = $request->name;
            $worker->email = $request->email;
            $worker->password = bcrypt($request->password);
            $worker->mobile = $request->mobile;
            $worker->address = $request->address;
            $worker->status = 1;
            $worker->save();

            $activity = new ActivityLog();
            $activity->description = '(freelancer) registration successfully';
            $activity->table = "worker_infos";
            $activity->id_no =$worker->worker_id ;
            $activity->user = $worker->name;
            $activity->save();

            $token = $worker->createToken('Se4So')->accessToken;

            return response()->json(['token' => $token, 'worker' => $worker ], 200);
    }

    public function apiLoginFreelancer(Request $request) {
        $this->validate($request, [
            'id_mobile' => 'required',
            'password' => 'required',
        ]);
        $freelancer = new WorkerInfo();
        // query and check by input mobile number
        $workerMobile = WorkerInfo::where('mobile',$request->id_mobile)->first();
       if ($workerMobile ) {
           //mobile is ok
           if (password_verify($request->password,$workerMobile->password)) {
               $activity = new ActivityLog();
               $activity->description = '(freelancer) login successfully';
               $activity->table = "worker_infos";
               $activity->id_no =$workerMobile->worker_id ;
               $activity->user = $workerMobile->name;
               $activity->save();
               $token = $freelancer->createToken('Se4So')->accessToken;
               return response()->json(['token' => $token , 'worker' => $workerMobile ], 200);
           }
       }// query and check by input mobile number

        // query and check by input worker id
        $workerId = WorkerInfo::where('worker_id',$request->id_mobile)->first();
       if ($workerId ) {
           //worker id is ok
           if (password_verify($request->password,$workerId->password)) {
               $activity = new ActivityLog();
               $activity->description = '(freelancer) login successfully';
               $activity->table = "worker_infos";
               $activity->id_no =$workerId->worker_id ;
               $activity->user = $workerId->name;
               $activity->save();
               $token = $freelancer->createToken('Se4So')->accessToken;
               return response()->json(['token' => $token , 'worker' => $workerId ], 200);
           }
       }// query and check by input worker id

       return 'error';

    }

    public function apiLogoutFreelancer(Request $request ) {
        return 'logout';
    }

    public function apiFreelancerInfoById($workerId ) {
        return WorkerInfo::where('worker_id',$workerId)->first();
        return response()->json(['worker' => $worker ], 200);
    }

    public function apiFreelancerImageUpdate(Request $request ) {
       // \Log::info($request->all());
        $image = $request->image;
        $imageName = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
        $exploded = explode(',', $imageName );
//         base64_decode($exploded[1]);
        if (str_contains($exploded[0],'jpeg') || str_contains($exploded[0],'png')) {
            Image::make($request->image)->save(public_path('front/profile-images/').$imageName);

            $worker = WorkerInfo::where('worker_id',$request->worker_id)->first();
            $worker->image = 'front/profile-images/'.$imageName;
            $worker->save();

            $activity = new ActivityLog();
            $activity->description = '(freelancer) update profile image successfully';
            $activity->table = "worker_infos";
            $activity->id_no =$worker->worker_id ;
            $activity->user = $worker->name;
            $activity->save();

            return $worker;
        } else {
            //wrong file format
            return 'wrong';
        }

    }

    public function apiFreelancerSkillFetch() {
        return Category::where('status',1)->get();
    }

    public function apiFreelancerServiceFetchById($id) {
        return Service::where('status',1)->where('category_id',$id)->get();
    }

    public function apiFreelancerServiceFeatureFetchByServiceId($id) {
        return ServiceFeature::where('status',1)->where('service_id',$id)->get();
    }

    public function apiPublishThanas() {
        return DB::table('divisions')
            ->join('thanas','divisions.id','=','thanas.division')
            ->select('divisions.*','thanas.*')
            ->get();
    }

    public function apiServiceFeaturePriceById($id) {
        return ServiceFeature::find($id);
    }

    public function apiSaveFreelancerJobField(Request $request) {
         //\Log::info($request->all());
         //return $request->all();
//        $selectAreas = ($request->input('selectArea'));
//        foreach ($selectAreas as $area ) {
//          return $area;
//        }
//

        $count = FreelancerJobField::where('worker_id',$request->worker_id)
            ->where('category_id',$request->category_id)
            ->where('service_id',$request->service_id)
            ->where('service_feature_id',$request->service_feature_id)
            ->count();
        if ($count != 0 ) {
            return 'duplicateJobField';
        }
        $jobField = new FreelancerJobField();
        $jobField->worker_id = $request->worker_id;
        $jobField->category_id = $request->category_id;
        $jobField->service_id = $request->service_id;
        $jobField->service_feature_id = $request->service_feature_id;
        $jobField->price = $request->price;
        $jobField->save();

        $selectAreas = $request->input('selectArea');
        foreach ($selectAreas as $area ) {
           $freelancerArea = new FreelancerWorkingArea();
           $thana = Thana::find($area);
           $division = Division::find($thana->division);
           $freelancerArea->worker_id = $request->worker_id;
           $freelancerArea->service_id = $request->service_id;
           $freelancerArea->service_feature_id = $request->service_feature_id;
           $freelancerArea->division_id = $division->id;
           $freelancerArea->thana_id = $area;
           $freelancerArea->save();
        }

        $worker = WorkerInfo::where('worker_id',$request->worker_id)->first();
        $activity = new ActivityLog();
        $activity->description = '(freelancer) add new job filed';
        $activity->table = "freelancer_job_fields";
        $activity->id_no =$request->worker_id ;
        $activity->user = $worker->name;
        $activity->save();

        return response()->json(['jobField' => $jobField], 200);
    }

    public function apiShowFreelancerJobField($workerId) {
        $jobFields = DB::table('freelancer_job_fields')
            ->join('categories','freelancer_job_fields.category_id','=','categories.id')
            ->join('services','freelancer_job_fields.service_id','=','services.id')
            ->join('service_features','freelancer_job_fields.service_feature_id','=','service_features.id')
            ->where('freelancer_job_fields.worker_id',$workerId)
            ->orderBy('freelancer_job_fields.id','desc')
            ->select('freelancer_job_fields.*','categories.category_name','services.service_name','service_features.service_feature')
            ->get();
        $categories = DB::table('categories')->where('status',1)->get();
        $jobAreas = DB::table('freelancer_working_areas')
            ->join('thanas','freelancer_working_areas.thana_id','=','thanas.id')
            ->where('freelancer_working_areas.worker_id',$workerId)
            ->select('freelancer_working_areas.*','thanas.*')->get();
        return response()->json(['jobFields' => $jobFields, 'categories' => $categories, 'jobAreas' => $jobAreas, ], 200);
    }

    public function apiFreelancerEditJobField($categoryId, $serviceId, $serviceFeatureId,$price, $workerId) {
        $freelancerJobField = DB::table('freelancer_job_fields')
            ->join('categories','freelancer_job_fields.category_id','=','categories.id')
            ->join('services','freelancer_job_fields.service_id','=','services.id')
            ->join('service_features','freelancer_job_fields.service_feature_id','=','service_features.id')
            ->where('freelancer_job_fields.worker_id',$workerId)
            ->where('freelancer_job_fields.category_id',$categoryId)
            ->where('freelancer_job_fields.service_id',$serviceId)
            ->where('freelancer_job_fields.service_feature_id',$serviceFeatureId)
            ->where('freelancer_job_fields.price',$price)
            ->orderBy('freelancer_job_fields.id','desc')
            ->select('freelancer_job_fields.*','categories.category_name','services.service_name','service_features.service_feature')
            ->get();
        $services = DB::table('services')->where('status',1)->get();
        $categories = DB::table('categories')->where('status',1)->get();
        return response()->json(['freelancerJobField' => $freelancerJobField, 'services' => $services, 'categories' => $categories], 200);
    }

    public function apiFreelancerUpdateJobField(Request $request) {
//        return $request->all();
        $count = FreelancerJobField::where('worker_id',$request->worker_id)
            ->where('category_id',$request->category_id)
            ->where('service_id',$request->service_id)
            ->where('service_feature_id',$request->service_feature_id)
            ->count();
        if ($count != 0 ) {
            return 'duplicateJobField';
        }
        FreelancerJobField::find($request->job_field_id)->delete();
        $jobField = new FreelancerJobField();
        $jobField->worker_id = $request->worker_id;
        $jobField->category_id = $request->category_id;
        $jobField->service_id = $request->service_id;
        $jobField->service_feature_id = $request->service_feature_id;
        $jobField->price = $request->price;
        $jobField->save();

        $worker = WorkerInfo::where('worker_id',$request->worker_id)->first();
        $activity = new ActivityLog();
        $activity->description = '(freelancer) update job filed';
        $activity->table = "freelancer_job_fields";
        $activity->id_no =$request->worker_id ;

        $activity->user = $worker->name;
        $activity->save();
        return 'ok';
    }

    public function apiFreelancerDeleteJobField(Request $request) {
        FreelancerJobField::find($request->delete_job_field_id)->delete();
        return 'done';
    }

    public function apiPublishServiceForHome() {
       return DB::table('service_point_for_services')
            ->join('services','service_point_for_services.service_id','=','services.id')
            ->join('service_points','service_point_for_services.service_point_id','=','service_points.id')
            ->where('service_point_for_services.service_point_id',1)
            ->orderBy('service_point_for_services.id','desc')
            ->select('services.*','service_point_for_services.*')
            ->get();
    }

    public function apiPublishServiceForOffice() {
       return DB::table('service_point_for_services')
            ->join('services','service_point_for_services.service_id','=','services.id')
            ->join('service_points','service_point_for_services.service_point_id','=','service_points.id')
            ->where('service_point_for_services.service_point_id',4)
            ->orderBy('service_point_for_services.id','desc')
            ->select('services.*','service_point_for_services.*')
            ->get();
    }


    public function apiPublishDivisions() {
       return Division::where('status',1)->get();
    }

}
