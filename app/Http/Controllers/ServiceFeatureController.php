<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ServiceFeature;
use App\ActivityLog;
use Illuminate\Support\Facades\Session;
use Image;

class ServiceFeatureController extends Controller
{
    public function serviceFeatureForm() {
        return view('admin.service-feature..show-service-feature-form');
    }

    public function createServiceFeature(Request $request)
    {
        //return $request->all();
        $this->validate($request, [
            'service_id' => 'required',
            'service_feature' => 'required',
            'price' => 'required',
            'description' => 'required',
            'image' => 'required',
            'status' => 'required'
        ]);

        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $directory = 'admin/service-feature-images/';
        $imageUrl = $directory . $imageName;
        Image::make($image)->save($imageUrl);

        $feature = new ServiceFeature();
        $feature->service_id = $request->service_id;
        $feature->service_feature = $request->service_feature;
        $feature->price = $request->price;
        $feature->description = $request->description;
        $feature->image = $imageUrl;
        $feature->status = $request->status;

        $feature->save();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have created new entry successfully";
        $activity->table = "service_feature";
        $activity->id_no =$feature->id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service-feature/service-feature-form')->with('message', 'Service Feature added successfully');
    }


    public function manageServiceFeature()
    {
        return view('admin.service-feature.service-feature-manage');
    }

    public function unpublishedServiceFeature($id)
    {
        DB::table('service_features')->where('id', $id)->update(['status' => 0]);

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have unpublished entry successfully";
        $activity->table = "service_feature";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service-feature/service-feature-table')->with('message', 'Unpublished service feature info successfully');
    }

    public function publishedServiceFeature($id)
    {
        DB::table('service_features')->where('id', $id)->update(['status' => 1]);

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have published entry successfully";
        $activity->table = "service_feature";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service-feature/service-feature-table')->with('message', 'Published service feature info successfully');
    }

    public function serviceFeatureEditableForm($id)
    {
        $featureById = ServiceFeature::find($id);
        return view('admin.service-feature.editable-service-feature-form', [
            'featureById' => $featureById
        ]);
    }

    public function serviceFeatureInfoUpdate(Request $request)
    {
        $this->validate($request, [
            'service_id' => 'required',
            'service_feature' => 'required',
            'price' => 'required',
            'description' => 'required',
            'status' => 'required'
        ]);

        if ($request->file('image')==null ) {
            $feature = ServiceFeature::find($request->featureId);
            $feature->service_id = $request->service_id;
            $feature->service_feature = $request->service_feature;
            $feature->price = $request->price;
            $feature->description = $request->description;
            $feature->status = $request->status;
            $feature->save();

        } else {

            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $directory = 'admin/service-feature-images/';
            $imageUrl = $directory . $imageName;
            Image::make($image)->save($imageUrl);

            $feature = ServiceFeature::find($request->featureId);
            $feature->service_id = $request->service_id;
            $feature->service_feature = $request->service_feature;
            $feature->price = $request->price;
            $feature->description = $request->description;
            $feature->image = $imageUrl;
            $feature->status = $request->status;
            $feature->save();
        }

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have updated entry successfully";
        $activity->table = "service_feature";
        $activity->id_no =$feature->id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service-feature/service-feature-table')->with('message', 'Update service feature info successfully');

    }

    public function serviceFeatureInfoDelete($id )
    {
        ServiceFeature::find($id)->delete();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have deleted entry successfully";
        $activity->table = "service_feature";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();
        return redirect('/admin-panel/service-feature/service-feature-table')->with('message', 'Service Feature info delete successfully');
    }
}
