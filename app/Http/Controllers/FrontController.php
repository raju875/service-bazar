<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function homePage() {
        return view('front.home.home');
    }

    public function servicePage() {
        return view('front.service.service-page');
    }
}
