<?php

namespace App\Http\Controllers;

use App\Skill;
use App\User;
use App\WorkerInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Image;

class FrontRegistrationLoginController extends Controller
{
    public function signupPage() {
        return view('front.login.signup-page');
    }

    public function workerRegisterFirstPage() {
        return view('front.login.worker-register-page');
    }

    public function workerRegisterSecondPage(Request $request) {
        $this->validate($request,[
            'name'=>'required',
            'password'=>'required',
            're_password'=>'required',
            'mobile'=>'required|size:11|regex:/(01)[0-9]{9}/',

        ]);
        //return $request->all();
        if ($request->password != $request->re_password ) {
            return redirect('/worker-register1')->with('alert','Password confirmation is not correct!!!');
        }
       Session::put('wName',$request->name);
        if ($request->email){
            Session::put('wEmail',$request->email);
        }

        Session::put('wPassword',$request->password);
        Session::put('wMobile',$request->mobile);
        return view('front.login.worker-address-page');
    }

    public function ajaxThanaList($id ) {
        $thanas = DB::table("thanas")->where("division",$id)->get();
//        return json_encode($subCategories);
        //return $subCategories;
        foreach ($thanas as $thana ) {
            echo "<option  value=$thana->id>$thana->thana </option>";
        }
    }

    public function workerRegisterThirdPage(Request $request) {
        //return $request->all();
        $this->validate($request,[
            'division'=>'required',
            'thana'=>'required',
        ]);
        Session::put('wDivision',$request->division);
        Session::put('wThana',$request->thana);
        return view('front.login.personal-info-page');
    }

    public function saveInfo(Request $request) {
        //return $request->all();
        $this->validate($request,[
            'address'=>'required',
            'skill'=>'required',
            'image'=>'required',
        ]);
        $mobile = Session::get('wMobile');
        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $directory = 'front/worker-images/';
        $imageUrl = $directory . $imageName;
        Image::make($image)->save($imageUrl);

       $count = WorkerInfo::count();
       $id = 'se4so-'.($count+1);
       $worker = new WorkerInfo();
       $worker->worker_id = $id;
       $worker->name = Session::get('wName');
       $worker->email = Session::get('wEmail');
       $worker->password = bcrypt(Session::get('wPassword'));
       $worker->mobile = Session::get('wMobile');
       $worker->division_id = Session::get('wDivision');
       $worker->thana_id = Session::get('wThana');
       $worker->address = $request->address;
       $worker->image = $imageUrl;
       $worker->save();

        $skills = $request->input('skill');
        foreach ($skills as $skill) {
            $mySkill = new Skill();
            $mySkill->worker_id = $id;
            $mySkill->skill = $skill;
            $mySkill->save();
        }
       Session::put('wId',$worker->id);

        return redirect('/my-profile');
    }

    public function showMyProfile() {
        return view('front.profile.my-profile');
    }

    public function userRegisterPage() {
        return view('front.login.user-register');
    }

    public function userInfoSave(Request $request) {
        $this->validate($request,[
            'name'=>'required',
            'password'=>'required',
            're_password'=>'required',
            'mobile'=>'required|size:11|regex:/(01)[0-9]{9}/',

        ]);
        //return $request->all();
        if ($request->password != $request->re_password ) {
            return redirect('/user-register')->with('alert','Password confirmation is not correct!!!');
        }
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->mobile = $request->mobile;
        $user->save();
        Session::put('userId',$user->id);
        return redirect('/');
    }

    public function userSignout() {
        Session::forget('userId');
        return redirect('/');
    }

    public function login() {
        return view('front.login.login');
    }

    public function loginSubmit(Request $request) {
        $count = User::where('email',$request->email)->count();
        if ($count == 0 ) {
            return redirect('/login')->with('alert','Wrong email!!! try again');
        } else {
            $user = User::where('email',$request->email)->first();
            if ($user ) {
                if (password_verify($request->password,$user->password)) {
                    Session::put('userId',$user->id);
                    return redirect('/');
                }  else {
                    return redirect('/login')->with('alert','Wrong password!!!try again');
            }

        }
    }


}}
