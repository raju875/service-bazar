<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class ApiFooterController extends Controller
{
    public function apiFooterAllPublishCategory () {
        return Category::where('status',1)->get();
    }
}
