<?php

namespace App\Http\Controllers;

use App\WorkerInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\ActivityLog;

class WorkerController extends Controller
{
    public function listOfWorker() {
        return view('admin.worker.worker-list');
    }


     public function unpublishedWorker($id) {
         DB::table('worker_infos')->where('id', $id)->update(['status' => 0]);

         $activity = new ActivityLog();
         $activity->description = Session::get('adminName')." has unpublished worker info successfully";
         $activity->table = "worker_infos";
         $activity->id_no =$id ;
         $activity->user = Session::get('adminEmail');
         $activity->save();
         return redirect('/admin-panel/worker/worker-list')->with('message', 'Unpublished worker info successfully');
    }


    public function publishedWorker($id) {
        DB::table('worker_infos')->where('id', $id)->update(['status' => 1]);
        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." has published worker info successfully";
        $activity->table = "worker_infos";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();
        return redirect('/admin-panel/worker/worker-list')->with('message', 'Published worker info successfully');
    }

    public function workerInfoDelete($id) {
        DB::table('worker_infos')->where('id', $id)->delete();
        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." has deleted worker info successfully";
        $activity->table = "worker_infos";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();
        return redirect('/admin-panel/worker/worker-list')->with('message', 'Delete worker info successfully');
    }




    public function workerDetails($id ) {
        return view('admin.worker.worker-details',[
            'id' => $id
        ]);
    }


    public function workerEditableForm($id ) {
        return view('admin.worker.worker-editable-form', [
            'id' => $id
        ]);
    }

}
