<?php

namespace App\Http\Controllers;

use App\Division;
use App\Service;
use App\Thana;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiSearchController extends Controller
{
    public function apiQueryForGlobalSearching() {
        return DB::table('freelancer_working_areas')
            ->join('thanas','freelancer_working_areas.thana_id','=','thanas.id')
            ->join('services','freelancer_working_areas.service_id','=','services.id')
            ->join('service_features','freelancer_working_areas.service_feature_id','=','service_features.id')
           // ->join('categories','services.category_id','=','categories.id')
            ->join('divisions','freelancer_working_areas.division_id','=','divisions.id')
            ->select('thanas.*','services.*','divisions.*','service_features.*')
            ->where('thanas.status',1)
            ->where('divisions.status',1)
            ->where('services.status',1)
            //->groupBy('services.id')
            ->get();
       return Service::all();
    }

    public function apiLocationSelectByDivision($divisionName) {
        $division = Division::where('division_name',$divisionName)->first();
        return DB::table('freelancer_working_areas')
            ->join('thanas','freelancer_working_areas.thana_id','=','thanas.id')
            ->join('services','freelancer_working_areas.service_id','=','services.id')
            ->join('divisions','freelancer_working_areas.division_id','=','divisions.id')
            ->select('thanas.*','services.*','divisions.*')
            ->where('freelancer_working_areas.division_id',$division->id)
            ->get();
    }


    public function apiLocationSelectByThana($divisionName, $thanaName) {
        $division = Division::where('division_name',$divisionName)->first();
        $thana = Thana::where('thana',$thanaName)->first();
        $locations = DB::table('freelancer_working_areas')
            ->join('thanas','freelancer_working_areas.thana_id','=','thanas.id')
            ->join('services','freelancer_working_areas.service_id','=','services.id')
            ->join('divisions','freelancer_working_areas.division_id','=','divisions.id')
            ->select('thanas.*','services.*','divisions.*')
            ->where('freelancer_working_areas.division_id',$division->id)
            ->where('freelancer_working_areas.thana_id',$thana->id)
            ->get();
        return response()->json(['division' => $division, 'thana' => $thana, 'locations' => $locations, ], 200);

    }
}
