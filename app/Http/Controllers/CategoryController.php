<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use App\Category;
use Illuminate\Support\Facades\DB;
use App\ActivityLog;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    public function categoryForm() {
        return view('admin.category.show-category-form');
    }

        public function createCategory(Request $request)
        {
            //return $request->all();
            $this->validate($request, [
                'category_name' => 'required',
                'category_icon' => 'required',
                'status' => 'required'
            ]);

            $icon = $request->file('category_icon');
            $iconName = $icon->getClientOriginalName();
            $directory = 'admin/category-icons/';
            $iconUrl = $directory . $iconName;
            Image::make($icon)->save($iconUrl);


            $category = new Category();
            $category->category_name = $request->category_name;
            $category->category_icon = $iconUrl;
            $category->description = $request->description;
            $category->status = $request->status;
            $category->save();

            $activity = new ActivityLog();
            $activity->description = Session::get('adminName')." have created new entry successfully";
            $activity->table = "categories";
            $activity->id_no =$category->id ;
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/admin-panel/category/category-form')->with('message', 'Category added successfully');
        }

    public function manageCategory()
    {
        $allCategories = DB::table('categories')->orderBy('id', 'desc')->get();
        return view('admin.category.category-manage', [
            'allCategories' => $allCategories
        ]);
    }

    public function unpublishedCategory($id)
    {
        DB::table('categories')->where('id', $id)->update(['status' => 0]);
        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have unpublished entry successfully";
        $activity->table = "categories";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();
        return redirect('/admin-panel/category/category-table')->with('message', 'Unpublished category info successfully');
    }

    public function publishedCategory($id)
    {
        DB::table('categories')->where('id', $id)->update(['status' => 1]);
        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have published entry successfully";
        $activity->table = "categories";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();
        return redirect('/admin-panel/category/category-table')->with('message', 'Published category info successfully');
    }

    public function categoryEditableForm($id)
    {
        $categoryById = Category::find($id);
        return view('admin.category.editable-category-form', [
            'categoryById' => $categoryById
        ]);
    }

    public function categoryInfoUpdate(Request $request)
    {
        // return $request->category_name;

        if ($request->file('category_icon')==null ) {
            $category = Category::find($request->id);
            $category->category_name = $request->category_name;
            $category->description = $request->description;
            $category->status = $request->status;
            $category->save();

            $activity = new ActivityLog();
            $activity->description = Session::get('adminName')." have updated entry successfully";
            $activity->table = "categories";
            $activity->id_no =$category->id ;
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/admin-panel/category/category-table')->with('message', 'Update category info successfully');

        } else {

            $icon = $request->file('category_icon');
            $iconName = $icon->getClientOriginalName();
            $directory = 'admin/category-icons/';
            $iconUrl = $directory . $iconName;
            Image::make($icon)->save($iconUrl);


            $category = Category::find($request->id);
            $category->category_name = $request->category_name;
            $category->description = $request->description;
            $category->category_icon = $iconUrl;
            $category->status = $request->status;
            $category->save();

            $activity = new ActivityLog();
            $activity->description = Session::get('adminName')." have updated entry successfully";
            $activity->table = "categories";
            $activity->id_no =$category->id ;
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/admin-panel/category/category-table')->with('message', 'Update category info successfully');


        }
    }

    public function categoryInfoDelete($id )
    {
        Category::find($id)->delete();
        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have deleted entry successfully";
        $activity->table = "categories";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();
        return redirect('/admin-panel/category/category-table')->with('message', 'Category info delete successfully');
    }
}
