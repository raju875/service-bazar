<?php

namespace App\Http\Controllers;

use App\Thana;
use Illuminate\Http\Request;
use App\division;
use Illuminate\Support\Facades\DB;
use App\ActivityLog;
use Illuminate\Support\Facades\Session;

class AreaController extends Controller
{
    public function divisionForm() {
        return view('admin.area.division.show-division-form');
    }

    public function thanaForm() {
        //return Division::all();
        return view('admin.area.thana.show-thana-form');
    }

    public function createDivision(Request $request)
    {
        //return $request->all();
        $this->validate($request, [
            'division_name' => 'required',
            'status' => 'required'
        ]);

        $division = new Division();
        $division->division_name = $request->division_name;
        $division->status = $request->status;

        $division->save();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have created new entry successfully";
        $activity->table = "division";
        $activity->id_no =$division->id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/area/division-form')->with('message', 'Division added successfully');
    }

    public function createThana(Request $request)
    {
        //return $request->all();
        $this->validate($request, [
            'division' => 'required',
            'thana' => 'required',
            'status' => 'required'
        ]);

        $thana = new Thana();
        $thana->division = $request->division;
        $thana->thana = $request->thana;
        $thana->status = $request->status;

        $thana->save();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have created new entry successfully";
        $activity->table = "thana";
        $activity->id_no =$thana->id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/area/thana-form')->with('message', 'Thana added successfully');
    }

    public function manageDivision()
    {
        $allDivisions = DB::table('divisions')->orderBy('id', 'desc')->get();
        return view('admin.area.division.division-manage', [
            'allDivisions' => $allDivisions
        ]);
    }

    public function manageThana()
    {
        $allThanas = DB::table('thanas')->orderBy('id', 'desc')->get();
        return view('admin.area.thana.thana-manage', [
            'allThanas' => $allThanas
        ]);
    }

    public function unpublishedDivision($id)
    {
        DB::table('divisions')->where('id', $id)->update(['status' => 0]);

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have unpublished entry successfully";
        $activity->table = "division";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/area/division-table')->with('message', 'Unpublished division info successfully');
    }

    public function unpublishedThana($id)
    {
        DB::table('thanas')->where('id', $id)->update(['status' => 0]);

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have unpublished  entry successfully";
        $activity->table = "thana";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/area/thana-table')->with('message', 'Unpublished thana info successfully');
    }

    public function publishedDivision($id)
    {
        DB::table('divisions')->where('id', $id)->update(['status' => 1]);

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have published entry successfully";
        $activity->table = "division";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();
        return redirect('/admin-panel/area/division-table')->with('message', 'Published division info successfully');
    }

    public function publishedThana($id)
    {
        DB::table('thanas')->where('id', $id)->update(['status' => 1]);

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have published entry successfully";
        $activity->table = "thana";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/area/thana-table')->with('message', 'Published thana info successfully');
    }

    public function divisionEditableForm($id)
    {
        $divisionByid = Division::find($id);
        return view('admin.area.division.editable-division-form', [
            'divisionByid' => $divisionByid
        ]);
    }

    public function thanaEditableForm($id)
    {
        $thanaByid = Thana::find($id);
        return view('admin.area.thana.editable-thana-form', [
            'thanaByid' => $thanaByid
        ]);
    }

    public function divisionInfoUpdate(Request $request)
    {
        // return $request->division_name;
            $division = Division::find($request->id);
            $division->division_name = $request->division_name;
            $division->status = $request->status;
            $division->save();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have updated entry successfully";
        $activity->table = "division";
        $activity->id_no =$division->id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

            return redirect('/admin-panel/area/division-table')->with('message', 'Published division info successfully');

    }

    public function thanaInfoUpdate(Request $request)
    {
         //return $request->all();
        $thana = Thana::find($request->id);
        $thana->division = $request->division;
        $thana->thana = $request->thana;
        $thana->status = $request->status;
        $thana->save();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have updated entry successfully";
        $activity->table = "thana";
        $activity->id_no =$thana->id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();
            return redirect('/admin-panel/area/thana-table')->with('message', 'Update thana info successfully');
    }

    public function divisionInfoDelete($id )
    {
        Division::find($id)->delete();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have deleted entry successfully";
        $activity->table = "division";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/area/division-table')->with('message', 'division info delete successfully');
    }

    public function thanaInfoDelete($id )
    {
        Thana::find($id)->delete();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have deleted entry successfully";
        $activity->table = "thana";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/area/thana-table')->with('message', 'Thana info delete successfully');
    }
}
