<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\DB;
use App\ActivityLog;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class BannerController extends Controller
{
    public function bannerForm() {
        return view('admin.banner.show-banner-form');
    }

    public function createBanner(Request $request)
    {
        //return $request->all();
        $this->validate($request, [
            'banner' => 'required',
            'status' => 'required'
        ]);

        $banner = $request->file('banner');
        $bannerName = $banner->getClientOriginalName();
        $directory = 'admin/banner-images/';
        $bannerUrl = $directory . $bannerName;
        Image::make($banner)->save($bannerUrl);

        $banner = new Banner();
        $banner->banner_name = $request->banner_name;
        $banner->description = $request->description;
        $banner->banner = $bannerUrl;
        $banner->status = $request->status;
        $banner->save();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have created new entry successfully";
        $activity->table = "banners";
        $activity->id_no =$banner->id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/banner/banner-form')->with('message', 'Banner added successfully');
    }

    public function manageBanner()
    {
        $allBanners = DB::table('banners')->orderBy('id', 'desc')->get();
        return view('admin.banner.banner-manage', [
            'allBanners' => $allBanners
        ]);
    }

    public function unpublishedBanner($id)
    {
        DB::table('banners')->where('id', $id)->update(['status' => 0]);

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have unpublished entry successfully";
        $activity->table = "banners";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/banner/banner-table')->with('message', 'Unpublished banner info successfully');
    }

    public function publishedBanner($id)
    {
        DB::table('banners')->where('id', $id)->update(['status' => 1]);

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have published entry successfully";
        $activity->table = "banners";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/banner/banner-table')->with('message', 'Published banner info successfully');
    }

    public function bannerEditableForm($id)
    {
        $bannerByid = Banner::find($id);
        return view('admin.banner.editable-banner-form', [
            'bannerByid' => $bannerByid
        ]);
    }

    public function bannerInfoUpdate(Request $request)
    {
        // return $request->banner_name;

        if ($request->file('banner')==null ) {
            $banner = Banner::find($request->id);
            $banner->banner_name = $request->banner_name;
            $banner->description = $request->description;
            $banner->status = $request->status;
            $banner->save();

            $activity = new ActivityLog();
            $activity->description = Session::get('adminName')." have updated entry successfully";
            $activity->table = "banners";
            $activity->id_no =$banner->id ;
            $activity->user = Session::get('adminEmail');
            $activity->save();
            return redirect('/admin-panel/banner/banner-table')->with('message', 'Update banner info successfully');
        }

        $banner = $request->file('banner');
        $bannerName = $banner->getClientOriginalName();
        $directory = 'admin/banner-images/';
        $bannerUrl = $directory . $bannerName;
        Image::make($banner)->save($bannerUrl);

        $banner = Banner::find($request->id);
        $banner->banner_name = $request->banner_name;
        $banner->description = $request->description;
        $banner->banner = $bannerUrl;
        $banner->status = $request->status;
        $banner->save();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have updated entry successfully";
        $activity->table = "banners";
        $activity->id_no =$banner->id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/banner/banner-table')->with('message', 'Published banner info successfully');
    }

    public function bannerInfoDelete($id )
    {
        Banner::find($id)->delete();
        $activity = new ActivityLog();
        $activity->description =  Session::get('adminName')."have deleted entry successfully";
        $activity->table = "banners";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();
        return redirect('/admin-panel/banner/banner-table')->with('message', 'Banner info delete successfully');
    }
}
