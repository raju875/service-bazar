<?php

namespace App\Http\Controllers;

use App\ServicePointForService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Service;
use App\ActivityLog;
use Illuminate\Support\Facades\Session;
use Image;

class ServiceController extends Controller
{
    public function serviceForm() {
        return view('admin.service.show-service-form');
    }

    public function createService(Request $request)
    {
        //return $request->all();
        $this->validate($request, [
            'category_id' => 'required',
            'service_name' => 'required',
            'service_point_id' => 'required',
            'service_image' => 'required',
            'status' => 'required'
        ]);
        $icon = $request->file('service_image');
        $iconName = $icon->getClientOriginalName();
        $directory = 'admin/service-icons/';
        $iconUrl = $directory . $iconName;
        Image::make($icon)->save($iconUrl);

        $service = new Service();
        $service->category_id = $request->category_id;
        $service->service_name = $request->service_name;
        $service->service_image = $iconUrl;
        $service->status = $request->status;

        $service->save();

        $servicePointIds = $request->input('service_point_id');
        foreach ($servicePointIds as $servicePointId) {
            $serviceServicePoint = new ServicePointForService();
            $serviceServicePoint->service_point_id = $servicePointId;
            $serviceServicePoint->service_id = $service->id;
            $serviceServicePoint->save();
        }

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have created two entries successfully";
        $activity->table = "service & service_point_fcr_service ";
        $activity->id_no =$service->id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service/service-form')->with('message', 'Service added successfully');
    }


    public function manageService()
    {
        return view('admin.service.service-manage');
    }

    public function unpublishedService($id)
    {
        DB::table('services')->where('id', $id)->update(['status' => 0]);

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have unpublished entry successfully";
        $activity->table = "service";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service/service-table')->with('message', 'Unpublished service info successfully');
    }

    public function publishedService($id)
    {
        DB::table('services')->where('id', $id)->update(['status' => 1]);

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have published entry successfully";
        $activity->table = "service";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service/service-table')->with('message', 'Published service info successfully');
    }

    public function serviceEditableForm($id)
    {
        $serviceByid = Service::find($id);
        return view('admin.service.editable-service-form', [
            'serviceByid' => $serviceByid
        ]);
    }

    public function serviceInfoUpdate(Request $request)
    {
        //return $request->all();
        if ($request->file('service_image')==null ) {
            $service = Service::find($request->id);
            $service->category_id = $request->category_id;
            $service->service_name = $request->service_name;
            $service->status = $request->status;
            $service->save();

            ServicePointForService::where('service_id',$request->id)->delete();

            $servicePointIds = $request->input('service_point_id');
            foreach ($servicePointIds as $servicePointId) {
                ServicePointForService::where('service_point_id')->delete();
                $serviceServicePoint = new ServicePointForService();
                $serviceServicePoint->service_point_id = $servicePointId;
                $serviceServicePoint->service_id = $service->id;
                $serviceServicePoint->save();
            }
        } else {
            $icon = $request->file('service_image');
            $iconName = $icon->getClientOriginalName();
            $directory = 'admin/service-icons/';
            $iconUrl = $directory . $iconName;
            Image::make($icon)->save($iconUrl);

            $service = Service::find($request->id);
            $service->category_id = $request->category_id;
            $service->service_name = $request->service_name;
            $service->service_image = $iconUrl;
            $service->status = $request->status;
            $service->save();

            $servicePointIds = $request->input('service_point_id');
            foreach ($servicePointIds as $servicePointId) {
                $serviceServicePoint = new ServicePointForService();
                $serviceServicePoint->service_point_id = $servicePointId;
                $serviceServicePoint->service_id = $service->id;
                $serviceServicePoint->save();
            }
        }
        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have updated entry successfully";
        $activity->table = "service";
        $activity->id_no =$service->id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service/service-table')->with('message', 'Update Service info successfully');

    }

    public function serviceInfoDelete($id )
    {
        Service::find($id)->delete();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have deleted entry successfully";
        $activity->table = "service";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service/service-table')->with('message', 'Service info delete successfully');
    }
}
