<?php

namespace App\Http\Controllers;

use App\ServiceFeature;
use Illuminate\Http\Request;
use App\Service;

class ApiServiceFeatureController extends Controller
{
    public function apiFetchServiceFeature($id) {
        $serviceFeatures = ServiceFeature::where('service_id',$id)->where('status',1)->get();
        $serviceById = Service::find($id);

       return response()->json(['serviceFeatures' => $serviceFeatures, 'serviceById' => $serviceById ], 200);

    }
}
