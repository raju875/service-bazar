<?php

namespace App\Http\Controllers;

use App\Division;
use App\Thana;
use Illuminate\Http\Request;
use App\ServiceFeature;
use Illuminate\Support\Facades\DB;

class ApiAddToCartController extends Controller
{
    public function apiFetchServiceFeatureById($id) {
        return  ServiceFeature::find($id);
    }

    public function apiFetchServiceLocation() {
       return DB::table('divisions')
            ->join('thanas','divisions.id','=','thanas.division')
            ->select('divisions.*','thanas.*')
            ->get();
    }

    public function apiFetchFreelancerBySelectLocation($location, $serviceId) {
      return DB::table('freelancer_working_areas')
            ->join('worker_infos','freelancer_working_areas.worker_id','=','worker_infos.worker_id')
            ->join('freelancer_job_fields','freelancer_working_areas.worker_id','=','freelancer_job_fields.worker_id')
            ->where('freelancer_job_fields.service_id',$serviceId)
            ->where('freelancer_working_areas.thana_id',$location)
            ->where('freelancer_job_fields.service_id',$serviceId)
            ->select('worker_infos.*','freelancer_job_fields.price')
            ->get();
    }

}
