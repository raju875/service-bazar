<?php

namespace App\Http\Controllers;

use App\ActivityLog;
use App\ServicePoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ServicePointController extends Controller
{
    public function servicePointForm() {
        return view('admin.service-point.show-service-point-form');
    }

    public function createServicePoint(Request $request)
    {
        // return $request->all();
        $this->validate($request, [
            'service_point' => 'required',
            'status' => 'required'
        ]);

        $servicePoint = new ServicePoint();
        $servicePoint->service_point = $request->service_point;
        $servicePoint->status = $request->status;

        $servicePoint->save();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have created new entry successfully";
        $activity->table = "service_points";
        $activity->id_no =$servicePoint->id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service-point/service-point-form')->with('message', 'Service point added successfully');
    }


    public function manageServicePoint()
    {
        return view('admin.service-point.service-point-manage');
    }

    public function unpublishedServicePoint($id)
    {
        DB::table('service_points')->where('id', $id)->update(['status' => 0]);

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have unpublished entry successfully";
        $activity->table = "service_points";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service-point/service-point-table')->with('message', 'Unpublished service point info successfully');
    }

    public function publishedServicePoint($id)
    {
        DB::table('service_points')->where('id', $id)->update(['status' => 1]);

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have published entry successfully";
        $activity->table = "service_points";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service-point/service-point-table')->with('message', 'Published service point info successfully');
    }

    public function servicePointEditableForm($id)
    {
        $servicePointByid = ServicePoint::find($id);
        return view('admin.service-point.editable-service-point-form', [
            'servicePointByid' => $servicePointByid
        ]);
    }

    public function servicePointInfoUpdate(Request $request)
    {
        // return $request->all();
        $servicePoint = ServicePoint::find($request->id);
        $servicePoint->service_point = $request->service_point;
        $servicePoint->status = $request->status;
        $servicePoint->save();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have updated entry successfully";
        $activity->table = "service_point";
        $activity->id_no =$servicePoint->id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();
        return redirect('/admin-panel/service-point/service-point-table')->with('message', 'Update Service point info successfully');

    }

    public function servicePointInfoDelete($id )
    {
        ServicePoint::find($id)->delete();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." have deleted entry successfully";
        $activity->table = "service_points";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/service-point/service-point-table')->with('message', 'Service point info delete successfully');
    }
}
