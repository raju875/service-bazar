<?php

namespace App\Http\Controllers;

use App\ActivityLog;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function userList () {
       return view('admin.user.user-list');
    }


    public function deleteUserInfoById ($id) {
        User::find($id)->delete();

        $activity = new ActivityLog();
        $activity->description = Session::get('adminName')." has delete user info successfully";
        $activity->table = "users";
        $activity->id_no =$id ;
        $activity->user = Session::get('adminEmail');
        $activity->save();

        return redirect('/admin-panel/user/user-list')->with('message', 'Delete user info successfully');
    }
}
