<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::get('adminEmail')) {
            if (Session::get('adminRole') == 'super-admin'){
                return $next($request);
            } else {
                return redirect('/404');
            }
        } else {
            return redirect('/admin-panel/login-form')->with('message','Login first');
        }
    }
}
