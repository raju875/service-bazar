@extends('admin.master')

@section('title')
    Se4So | Order List
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Order <small>Table</small></h3>
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('alert'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage<small>Order Table</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <table class="table table-bordered">
                                <thead>
                                <?php $i=1 ?>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Customer Name</th>
                                    <th>Customer Mobile</th>
                                    <th>Worker ID</th>
                                    <th>Location</th>
                                    <th>Category</th>
                                    <th>Service</th>
                                    <th>Service Feature</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ 'demo' }}</td>
                                        <td>{{ 'demo' }}</td>
                                        <td>{{ 'demo' }}</td>
                                        <td>{{ 'demo' }}</td>
                                        <td>{{ 'demo' }}</td>
                                        <td>{{ 'demo' }}</td>
                                        <td>{{ 'demo' }}</td>
                                        <td>{{ 'demo' }}</td>
                                    </tr>
                                    <?php $i++ ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection
