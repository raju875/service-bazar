@extends('admin.master')

@section('title')
    Se4So
    @endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3 class="align-content-center"> Profile</h3>
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('aleart'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Profile</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('/admin-panel/profile/update-profile') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10 "style="margin: 100px" >
                                            <?php $admin = \App\administration::find(Session::get('adminId')) ?>
                                            <img src="{{ asset('admin/profile-pictures/') }}/{{ $admin->avatar }}" style="width:150px; height: 150px; float: left; border-radius: 50%; margin-right: 25px "/>
                                            <h2>{{ $admin->name }}'s Profile</h2>
                                            <form enctype="multipart/form-data" action="{{ url('/manan-administration2018/update-profile') }}" method="POST">
                                                {{ csrf_field() }}
                                                <label>Update Profile Picture</label>
                                                <input type="file" name="avatar">
                                                <input type="submit" class="pull-right btb btn-sm btn-primary">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <br>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end form for validations -->







@endsection



