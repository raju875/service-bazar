@extends('admin.master')

@section('title')
    Se4So | Thana Create
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3 class="align-content-center">Add Thana Form</h3>
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('aleart'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add New Division</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('/admin-panel/area/thana-create') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Division <span style="color: red">*</span></label>
                                    <?php $divisions = \App\Division::where('status',1)->get() ?>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select  name="division" required="required" class="form-control col-md-8 col-xs-12">
                                            <option value="">{{ 'Select Division' }}</option>
                                            @foreach($divisions as $division)
                                                <option value="{{ $division->id }}">{{ $division->division_name }}</option>
                                            @endforeach
                                        </select>
                                        <span style="color: red">{{ $errors->has('division') ? $errors->first('division') : ' ' }}</span>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Thana  <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="thana" required="required" class="form-control col-md-7 col-xs-12">
                                        <span style="color: red">{{ $errors->has('thana') ? $errors->first('thana') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Status <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select  name="status" required="required" class="form-control col-md-8 col-xs-12">
                                            <option value="1">Published</option>
                                            <option value="0">Unpublished</option>
                                        </select>
                                        <span style="color: red">{{ $errors->has('status') ? $errors->first('status') : ' ' }}</span>

                                    </div>
                                </div>
                                <br>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button class="btn btn-primary" type="button">Cancel</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                        <button type="submit" class="btn btn-success" name="btn">Submit</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end form for validations -->

    <script type="text/javascript">
        function previewIcon(event) {
            var reader = new FileReader();
            var imageField = document.getElementById("icon-field");
            reader.onload = function () {
                if (reader.readyState==2 ) {
                    imageField.src = reader.result;
                }
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>

    <script type="text/javascript">
        function previewImage(event) {
            var reader = new FileReader();
            var imageField = document.getElementById("image-field");
            reader.onload = function () {
                if (reader.readyState==2 ) {
                    imageField.src = reader.result;
                }
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>
@endsection
