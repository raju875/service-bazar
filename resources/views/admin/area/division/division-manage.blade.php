@extends('admin.master')

@section('title')
   Se4So | Category Table
    @endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Categories <small>Table</small></h3>
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('aleart'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage<small>Division Table</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <table class="table table-bordered">
                                <thead>
                                <?php $i=1 ?>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Division Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($allDivisions as $division )
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $division->division_name }}</td>
                                    <td>
                                        @if($division->status == 1 )
                                            {{'Published'}}
                                        @else
                                            {{ 'Unpublished' }}
                                        @endif
                                    </td>

                                    <td>
                                        @if($division->status == 1 )
                                            <a href="{{ url('/admin-panel/area/unpublished-division/'.$division->id) }}" class="btn btn-success btn-xs" title="published">
                                                <span class="glyphicon glyphicon-arrow-up"></span>
                                            </a>
                                        @else
                                            <a href="{{ url('/admin-panel/area/published-division/'.$division->id) }}" class="btn btn-warning btn-xs" title="unpublished">
                                                <span class="glyphicon glyphicon-arrow-down"></span>
                                            </a>
                                        @endif

                                        <a href="{{  url('/admin-panel/area/editable-division-form/'.$division->id) }}" class="btn btn-primary btn-xs" title="edit">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                        <a href="{{  url('/admin-panel/area/delete-division-info/'.$division->id) }}" onclick="return confirm('Are you sure to delete it !!!')" class="btn btn-primary btn-xs" title="delete">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        {{--<a href="{{  url('/my-ecommerce-administration/category/view-category-info/'.$category->id) }}" class="btn btn-primary btn-xs" title="view">--}}
                                            {{--<span class="glyphicon glyphicon-search"></span>--}}
                                        {{--</a>--}}
                                    </td>

                                </tr>
                                    <?php $i++ ?>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection
