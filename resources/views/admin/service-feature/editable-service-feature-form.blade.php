@extends('admin.master')

@section('title')
    Se4So | Service Feature Form
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3 class="align-content-center">Edit Service Feature</h3>
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('aleart'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Service Feature</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('/admin-panel/service-feature/update-service-feature-info') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Service Name <span style="color: red">*</span></label>
                                   <input type="hidden" name="featureId" value="{{ $featureById->id }}">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Service Name <span style="color: red">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select  name="service_id" required="required" class="form-control col-md-8 col-xs-12">
                                                <?php $serviceById = \App\Service::find($featureById->service_id)?>
                                                <?php $categoryById = \App\Category::find($serviceById->category_id)?>
                                                <option value="{{ $serviceById->id }}">{{ $serviceById->service_name }} ( {{ $categoryById->category_name }} )</option>

                                                    <?php $serviceFeaturea = \App\ServiceFeature::where('status',1)->get() ?>
                                                    @foreach($serviceFeaturea as $feature )
                                                        @if($featureById->id != $feature->id )
                                                            <?php $service = \App\Service::find($feature->service_id)?>
                                                            <?php $serviceCategory = \App\Category::find($service->category_id)?>
                                                            <option value="{{ $feature->id }}">{{ $service->service_name }} ( {{ $serviceCategory->category_name }} )</option>
                                                        @endif
                                                @endforeach
                                            </select>
                                            <span style="color: red">{{ $errors->has('category') ? $errors->first('category') : ' ' }}</span>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Service Feature <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="service_feature" required="required" value="{{ $featureById->service_feature }}" class="form-control col-md-7 col-xs-12">
                                        <span style="color: red">{{ $errors->has('service_feature') ? $errors->first('service_feature') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Price <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="price" required="required" value="{{ $featureById->price }}" class="form-control col-md-7 col-xs-12">
                                        <span style="color: red">{{ $errors->has('price') ? $errors->first('price') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Description</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       <textarea  name="description" id="description" class="form-control" rows="10" cols="8"  required >
                                           {{ $featureById->description }}
                            </textarea>
                                        <span style="color: red">{{ $errors->has('description') ? $errors->first('description') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Image <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="image" class="form-control" onchange="previewImage(event)" >
                                        <img src="{{ asset($featureById->image ) }}" width="100" height="70" id="image-field">
                                        {{--<div id="previewPhoto"></div>--}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Status <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select  name="status" required="required" class="form-control col-md-8 col-xs-12">
                                            @if($featureById->status == 1 )
                                            <option value="1">Published</option>
                                            <option value="0">Unpublished</option>
                                                @else
                                                <option value="0">Unpublished</option>
                                                <option value="1">Published</option>
                                            @endif
                                        </select>
                                        <span style="color: red">{{ $errors->has('status') ? $errors->first('status') : ' ' }}</span>
                                    </div>
                                </div>
                                <br>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button class="btn btn-primary" type="button">Cancel</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                        <button type="submit" class="btn btn-success" name="btn">Submit</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end form for validations -->
@endsection


<script type="text/javascript">
    function previewImage(event) {
        var reader = new FileReader();
        var imageField = document.getElementById("image-field");
        reader.onload = function () {
            if (reader.readyState==2 ) {
                imageField.src = reader.result;
            }
        }
        reader.readAsDataURL(event.target.files[0]);
    }
</script>