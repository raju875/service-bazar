<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Se4So! | </title>

    <!-- Bootstrap -->
    <link href="{{ asset('admin/assets/vendors') }}/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('admin/assets/vendors') }}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('admin/assets/vendors') }}/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{ asset('admin/assets/vendors') }}/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('admin/assets/build') }}/css/custom.min.css" rel="stylesheet">
</head>

<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form action="{{ url('/admin-panel/administration-login') }}" method="post">
                    @csrf
                    <h1>Login Form</h1>
                    @if(Session::has('message'))
                        <h4 class="text text-center text-success">{{ Session::get('message') }}</h4>
                    @endif
                    @if(Session::has('alert'))
                        <h4 class="text text-center text-danger">{{ Session::get('alert') }}</h4>
                    @endif
                    <div>
                        <input type="email" name="email" class="form-control" placeholder="Email" required="" />
                    </div>
                    <div>
                        <input type="password" name="password" class="form-control" placeholder="Password" required="" />
                    </div>
                    <div>
                        <div>
                            <?php $first  = rand(1,3) ?>
                            <?php $second = rand(4,6) ?>
                            <?php $result = $first*$second ?>
                            <?php  echo $first.'*'.$second ?>
                            <input type="hidden" name="res1" value="{{ $result}}">
                            <input class="form-control" type='text' name="res2" required id=''></input>  @if(Session::has('alert'))<h5 class="text text-center text-danger">{{ Session::get('alert') }}</h5>@endif
                        </div>
                    </div>
                    <div>
                        <input type="submit" value="Log in" name="btn"/>
                        <a class="reset_pass" href="#">Lost your password?</a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        {{--<p class="change_link">New to site?--}}
                            {{--<a href="{{ url('/admin-panel/super-admin-registration-form') }}" class="to_register"> Create Account </a>--}}
                        {{--</p>--}}

                        <div class="clearfix"></div>
                        <br />

                        <div>
                            <h1><i class="fa fa-paw"></i> Se4So!</h1>
                            <p>©2018 All Rights Reserved. Se4So! is a Bootstrap 3 template. Privacy and Terms</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>

    </div>
</div>
</body>
</html>
