<div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
        <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Se4So !</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
        <div class="profile_pic">
            <?php $admin = \App\Administration::find(Session::get('adminId'))?>
            <img src="{{ asset('admin/profile-pictures/') }}/{{$admin->avatar}}" alt="..." class="img-circle profile_img">
        </div>
        <div class="profile_info">
            <span>Welcome,</span>
            <h2><?php echo $admin->name ?></h2>
        </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <h3>General</h3>
            <ui class="nav side-menu">
                <li><a href="{{ url('/admin-panel/home') }}"><i class="fa fa-home"></i> Home </a></li>
                <li><a href="{{ url('/admin-panel/list/activity-log') }}"><i class="fa fa-home"></i> Activity Log </a>
                </li>
                <li><a><i class="fa fa-edit"></i> Banner <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ url('/admin-panel/banner/banner-form') }}">Create Banner</a></li>
                        <li><a href="{{ url('/admin-panel/banner/banner-table') }}">Manage Banner</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-edit"></i> Area <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ url('/admin-panel/area/division-form') }}">Create Division</a></li>
                        <li><a href="{{ url('/admin-panel/area/division-table') }}">Manage Division</a></li>
                        <li><a href="{{ url('/admin-panel/area/thana-form') }}">Create Thana</a></li>
                        <li><a href="{{ url('/admin-panel/area/thana-table') }}">Manage Thana</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-edit"></i> Service <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ url('/admin-panel/category/category-form') }}">Category</a></li>
                        <li><a href="{{ url('/admin-panel/category/category-table') }}">Manage Category</a></li>
                        <li><a href="{{ url('/admin-panel/service-point/service-point-form') }}"> Service Point</a></li>
                        <li><a href="{{ url('/admin-panel/service-point/service-point-table') }}"> Manage Service Point</a></li>
                        <li><a href="{{ url('/admin-panel/service/service-form') }}"> Service</a></li>
                        <li><a href="{{ url('/admin-panel/service/service-table') }}"> Manage Service</a></li>
                        <li><a href="{{ url('/admin-panel/service-feature/service-feature-form') }}"> Service Feature</a></li>
                        <li><a href="{{ url('/admin-panel/service-feature/service-feature-table') }}"> Manage Service Feature</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-desktop"></i> Worker <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ url('/admin-panel/worker/worker-list') }}">Worker List</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-desktop"></i> User <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ url('/admin-panel/user/user-list') }}">User List</a></li>
                    </ul>
                </li>
                <li><a><i class="fa fa-desktop"></i> Order <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ url('/admin-panel/order/order-list') }}">Order List</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
    <div class="sidebar-footer hidden-small">
        <a data-toggle="tooltip" data-placement="top" title="Settings">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Lock">
            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
    </div>
    </div>
