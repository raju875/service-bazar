@extends('admin.master')

@section('title')
    Solemate | Worker Form
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3 class="align-content-center">Update Woeker Info</h3>
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('aleart'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <?php $worker = \App\WorkerInfo::find($id)?>
                            <h2>Update {{ $worker->name }}'s Info</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('/admin-panel/category/update-category-info') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="hidden" name="id" required="required" value="{{ $worker->id }}" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Worker Name <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="name" required="required" value="{{ $worker->name }}" class="form-control col-md-7 col-xs-12">
                                        <span style="color: red">{{ $errors->has('name') ? $errors->first('name') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Worker Email <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="email" required="required" value="{{ $worker->email }}" class="form-control col-md-7 col-xs-12">
                                        <span style="color: red">{{ $errors->has('email') ? $errors->first('email') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Mobile No <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="mobile" required="required" value="{{ $worker->mobile }}" class="form-control col-md-7 col-xs-12">
                                        <span style="color: red">{{ $errors->has('mobile') ? $errors->first('mobile') : ' ' }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Address</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       <textarea  name="address" id="address" class="form-control" rows="10" cols="8"  required >
                                           {{ $worker->address }}
                            </textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Image <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="image" class="form-control" onchange="previewIcon(event)" >
                                        <img src="{{ asset($worker->image ) }}" width="100" height="70" id="icon-field">
                                        {{--<div id="previewPhoto"></div>--}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?php $workerSkill = \App\Skill::where('worker_id',$worker->worker_id)->get()?>
                                    <?php $categories = \App\Category::where('status',1)->get()?>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Skill <span style="color: red">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12"  class="dropdown-menu col-sm-12" aria-labelledby="dropdownMenuButton">

                                                @foreach($categories as $category )
                                                    @foreach($workerSkill as $skill )
                                                    @if($category->id == $skill->skill )
                                                                <a class="dropdown-item" href="#"><input type="checkbox"
                                                                                                         name="skill[]"
                                                                                                         value="{{  $category->id }}">{{ $category->category_name }}</a>
                                                @endif
                                                        @endforeach
                                                    @endforeach

                                </div>

                                        <div class="md-form mb-5 mt-lg-5">
                                            <div class="form-group ">
                                                <label for="" class="col-sm-2 col-form-label">Skill<span
                                                        style="color: red"> *</span></label>
                                                <div class="dropdown col-sm-10">
                                                    <button class="btn btn-block btn-secondary dropdown-toggle" type="button"
                                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        Choose Skill
                                                    </button>

                                                    <?php $workerSkill = \App\Skill::where('worker_id',$worker->worker_id)->get()?>
                                                    <?php $categories = \App\Category::where('status',1)->get()?>
                                                    <div class="dropdown-menu col-sm-12" aria-labelledby="dropdownMenuButton">
                                                        @foreach($workerSkill as $skill )
                                                        @foreach($categories as $category)

                                                            <a class="dropdown-item" href="#"><input type="checkbox" <?php  ($category->id == $skill->skill ) ? 'checked':'unchecked' ;?>
                                                                                                     name="skill[]"
                                                                                                     value="{{  $category->id }}">{{ $category->category_name }}</a>
                                                            @endforeach
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                <br>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Status <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select  name="status" required="required" class="form-control col-md-8 col-xs-12">
                                            @if($worker->status==1 )
                                                <option value="1">Published</option>
                                                <option value="0">Unpublished</option>
                                            @else
                                                <option value="0">Unpublished</option>
                                                <option value="1">Published</option>
                                            @endif
                                        </select>
                                        <span style="color: red">{{ $errors->has('status') ? $errors->first('status') : ' ' }}</span>

                                    </div>
                                </div>
                                <br>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button class="btn btn-primary" type="button">Cancel</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                        <button type="submit" class="btn btn-success" name="btn">Update it</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end form for validations -->
    <script type="text/javascript">
        function previewIcon(event) {
            var reader = new FileReader();
            var iconField = document.getElementById("icon-field");
            reader.onload = function () {
                if (reader.readyState==2 ) {
                    iconField.src = reader.result;
                }
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>

    <script type="text/javascript">
        function previewImage(event) {
            var reader = new FileReader();
            var imageField = document.getElementById("image-field");
            reader.onload = function () {
                if (reader.readyState==2 ) {
                    imageField.src = reader.result;
                }
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>

@endsection
