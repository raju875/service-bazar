@extends('admin.master')

@section('title')
    Se4So | Worker Details
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Worker
                        <small> Details</small>
                    </h3>
                    <?php $worker = \App\WorkerInfo::find($id) ?>
                    <img src="{{ asset($worker->image) }}"
                         style="width:150px; height: 150px; float: left; border-radius: 50%; margin-right: 25px "
                         class="d-100 img-circle" alt="">
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('alert'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <h3> Account Info</h3>
                            <table class="table table-bordered">
                                <?php $i = 1 ?>
                                <tr>
                                    <td>ID</td>
                                    <td>{{ $worker->worker_id }}</td>
                                </tr>
                                <tr>
                                    <td>Worker Name</td>
                                    <td>{{ $worker->name }}</td>
                                </tr>
                                <tr>
                                    <td> Email</td>
                                    <td>{{ $worker->email }}</td>
                                </tr>
                                <tr>
                                    <td>Mobile No</td>
                                    <td>{{ $worker->mobile }}</td>
                                </tr>
                                <tr>
                                    <td>Division</td>
                                    <?php $division = \App\Division::find($worker->division_id)?>
                                    <td>{{ $division->division_name }}</td>
                                </tr>
                                <tr>
                                    <td>Thana</td>
                                    <?php $thana = \App\Thana::find($worker->thana_id)?>
                                    <td>{{ $thana->thana }}</td>
                                </tr>
                            </table>
                        </div>
                        <hr/>
                        <div class="x_content">
                            <h3>Skills</h3>
                            <table class="table table-bordered">
                                <?php $skills = \App\Skill::where('worker_id',$worker->worker_id)->get()?>
                                @foreach($skills as $skill )
                                <tr>
                                    <?php $mySkill = \App\Category::find($skill->skill )?>
                                    <td>{{ $mySkill->category_name }}</td>
                                </tr>
                                    @endforeach
                            </table>
                        </div>
                        <hr/>
                        <div class="x_content">
                            <h3>Personal Info</h3>
                            <table class="table table-bordered">
                                <tr>
                                    <td>{{ $worker->address }}</td>
                                </tr>
                            </table>
                        </div>
                        <hr/>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
    </div>
    <!-- /page content -->
@endsection
