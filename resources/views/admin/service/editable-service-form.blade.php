@extends('admin.master')

@section('title')
    Se4So | Editable Service Form
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3 class="align-content-center">Update Service Info</h3>
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('alert'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Service</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('/admin-panel/service/update-service-info') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="hidden" name="id" required="required" value="{{ $serviceByid->id }}" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Category <span style="color: red">*</span></label>
                                    <?php $cat = \App\Category::find($serviceByid->category_id) ?>
                                    <?php $categories = \App\Category::where('status',1)->get() ?>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select  name="category_id" required="required" class="form-control col-md-8 col-xs-12">
                                            @if(!empty($cat))
                                                <option value="{{ $cat->id }}">{{ $cat->category_name }}</option>
                                            @else
                                                <option value=""></option>
                                            @endif
                                            @foreach($categories as $category)
                                                @if($category->id != $cat->id )
                                                    <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <span style="color: red">{{ $errors->has('category') ? $errors->first('category') : ' ' }}</span>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Category Icon <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="service_image" class="form-control" onchange="previewIcon(event)" >
                                        <img src="{{ asset($serviceByid->service_image ) }}" width="100" height="70" id="icon-field">
                                        {{--<div id="previewPhoto"></div>--}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Service Point <span style="color: red">*</span></label>
                                    <select id="module" name="service_point_id[]" required multiple="multiple">
                                        <?php $servicePoints = \App\ServicePoint::where('status',1)->get() ?>
                                        @foreach($servicePoints as $point )
                                            <option value="{{ $point->id }}">{{ $point->service_point }}</option>
                                        @endforeach
                                    </select>
                                    <span style="color: red">{{ $errors->has('service_point_id') ? $errors->first('service_point_id') : ' ' }}</span>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Service<span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="service_name" required="required" value="{{ $serviceByid->service_name }}" class="form-control col-md-7 col-xs-12">
                                        <span style="color: red">{{ $errors->has('service_name') ? $errors->first('service_name') : ' ' }}</span>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Status <span style="color: red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select  name="status" required="required" class="form-control col-md-8 col-xs-12">
                                            @if($serviceByid->status==1 )
                                                <option value="1">Published</option>
                                                <option value="0">Unpublished</option>
                                            @else
                                                <option value="0">Unpublished</option>
                                                <option value="1">Published</option>
                                            @endif
                                        </select>
                                        <span style="color: red">{{ $errors->has('status') ? $errors->first('status') : ' ' }}</span>

                                    </div>
                                </div>
                                <br>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button class="btn btn-primary" type="button">Cancel</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                        <button type="submit" class="btn btn-success" name="btn">Update it</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- end form for validations -->
    <script type="text/javascript">
        function previewIcon(event) {
            var reader = new FileReader();
            var iconField = document.getElementById("icon-field");
            reader.onload = function () {
                if (reader.readyState==2 ) {
                    iconField.src = reader.result;
                }
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>

@endsection
