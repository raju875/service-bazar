@extends('admin.master')

@section('title')
    Se4So | Registration
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    @if(Session::has('message'))
                        <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                    @endif
                    @if(Session::has('aleart'))
                        <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                    @endif
                    <br>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div>

                        <div class="login_wrapper">


                            <section class="login_content">
                                <form method="POST" action="{{ url('/admin-panel/administration/administration-sign-up') }}">
                                    @csrf
                                    <h1>Create Account</h1>
                                    @if(Session::has('message'))
                                        <h4 class="text text-center text-success">{{ Session::get('message') }}</h4>
                                    @endif
                                    @if(Session::has('alert'))
                                        <h4 class="text text-center text-danger">{{ Session::get('alert') }}</h4>
                                    @endif
                                    <div>
                                        <input type="text" name="name" class="form-control" placeholder="Username" required="" />
                                    </div>
                                    <div>
                                        <input type="email" onblur="emailCheck(this.value)" name="email" class="form-control" placeholder="Email" required="" />
                                    </div>
                                    <div>
                                        <input type="password" name="password" class="form-control" placeholder="Password" required="" />
                                    </div>
                                    <div>
                                        <input type="password" name="repassword" class="form-control" placeholder="Retype Password" required="" />
                                    </div>
                                    <div>
                                        <input type="number" name="mobile" class="form-control" placeholder="Mobile No" required="" />
                                    </div>
                                    <div>
                                        <select  name="role" required="required" class="form-control">
                                                <option value="admin">Admin</option>
                                                <option value="super-admin">Super Admin</option>
                                        </select>
                                    </div>
                                    <div>

                                        <?php $first  = rand(1,3) ?>
                                        <?php $second = rand(4,6) ?>
                                        <?php $result = $first*$second ?>
                                        <?php  echo $first.'*'.$second ?>
                                        <input type="hidden" name="res1" value="{{ $result}}">
                                        <input class="form-control" type='text' name="res2" required id=''></input>
                                    </div>
                                    <div>
                                        <input type="submit"  name="submit">
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="separator">
                                        <div class="clearfix"></div>
                                        <br />

                                        <div>
                                            <h1><i class="fa fa-paw"></i> Se4So!</h1>
                                            <p>©2018 All Rights Reserved. Se4So! Privacy and Terms</p>
                                        </div>
                                    </div>
                                </form>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end form for validations -->

    <script type="text/javascript">
        function previewIcon(event) {
            var reader = new FileReader();
            var imageField = document.getElementById("icon-field");
            reader.onload = function () {
                if (reader.readyState==2 ) {
                    imageField.src = reader.result;
                }
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>

    <script type="text/javascript">
        function previewImage(event) {
            var reader = new FileReader();
            var imageField = document.getElementById("image-field");
            reader.onload = function () {
                if (reader.readyState==2 ) {
                    imageField.src = reader.result;
                }
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>
@endsection
