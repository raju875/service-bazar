@extends('admin.master')

@section('title')
Se4So | Banner Table
@endsection

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Notification <small>Table</small></h3>
                @if(Session::has('message'))
                <h3 class="text text-center text-success">{{ Session::get('message') }}</h3>
                @endif
                @if(Session::has('aleart'))
                <h3 class="text text-center text-danger">{{ Session::get('alert') }}</h3>
                @endif
                <br>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="clearfix"></div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Activity<small> Table</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table class="table table-bordered">
                            <thead>
                            <?php $i=1 ?>
                            <tr>
                                <th>Sl No</th>
                                <th>Table Name</th>
                                <th>Id No</th>
                                <th>Description</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $activities = \App\ActivityLog::orderBy('id','desc')->get()?>
                            @foreach($activities as $activity )
                                @if(Session::get('adminEmail') == $activity->user )
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $activity->table }}</td>
                                <td>{{ $activity->id_no }}</td>
                                <td>{{ $activity->description }}</td>
                                <td>{{ $activity->user }}</td>
                                <td>{{ $activity->created_at->format('Y-m-d') }}</td>
                                <td>{{ $activity->created_at->diffForHumans() }}</td>
                            </tr>
                            <?php $i++ ?>
                            @endif
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- /page content -->
@endsection
