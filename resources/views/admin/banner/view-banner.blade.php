@extends('admin.master')

@section('title')
    <title>Solemate | Banner Details</title>
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    Banner Name :  <small >{{ $category->banner_name }}</small></h3>
                    <br>
                    <br>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2> Image : </h2> <div style="padding-left: 120px"><img src="{{ asset($banner->banner) }}" width="220" height="170"></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                <?php $banner->description ?>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection
